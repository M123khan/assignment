import React, { Component } from 'react';
class Navbar extends Component {
    state = {
    };
    NavbarShow = () => {
        let mc = this.props.Match_counter;
        return (
            <React.Fragment>
                <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                    <a className="navbar-brand mx-3" href="#">Football Tournament</a>
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                                Number Of Matches<span className="badge rounded-pill bg-primary">{mc}</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </React.Fragment>
        );
    }
    render() {
        return this.NavbarShow();
    }
}
export default Navbar;