import React, { Component } from 'react';
import Navbar from './navbar';
class Main extends Component {
    state =
        {
            Match_counter: 0,
            Index: 0,
            Team_1: "",
            Team_2: "",
            Main_Index: 0,
            Goal_a: 0,
            Goal_b: 0,
            Result_arr: [],
            PointsTable_arr: [
                { Team: 'France', Played: 0, Won: 0, Lost: 0, Drawn: 0, GoalsFor: 0, Goalagainst: 0, Points: 0 },
                { Team: 'England', Played: 0, Won: 0, Lost: 0, Drawn: 0, GoalsFor: 0, Goalagainst: 0, Points: 0 },
                { Team: 'Brazil', Played: 0, Won: 0, Lost: 0, Drawn: 0, GoalsFor: 0, Goalagainst: 0, Points: 0 },
                { Team: 'Germany', Played: 0, Won: 0, Lost: 0, Drawn: 0, GoalsFor: 0, Goalagainst: 0, Points: 0 },
                { Team: 'Argentina', Played: 0, Won: 0, Lost: 0, Drawn: 0, GoalsFor: 0, Goalagainst: 0, Points: 0 },
            ],
        };
    Sort = (v,s) =>
    {
        
        if(v === 1)
        {
            let set = {...this.state};
            set.PointsTable_arr = set.PointsTable_arr.sort((v1,v2) => v1[s] > v2[s]?1:-1);
            this.setState(set);
        }
        else
        {
            let set = {...this.state};
            set.PointsTable_arr = set.PointsTable_arr.sort((v1,v2) => v1[s] < v2[s]?1:-1);
            this.setState(set);
        }

    }    
    ShowPointsTable = () => {
        let arr = this.state.PointsTable_arr;
        return (
            <React.Fragment>
                <div className="container">
                    {
                        <div className="mt-2">
                            <h6 className="display-6 text-center">Points Table</h6>
                            <div className="row mt-2 bg-dark text-light fw-bold">
                                <div className="col-3 text-center" onClick = {() => this.Sort(1,'Team')}>Team</div>
                                <div className="col-1 text-center" onClick = {() => this.Sort(0,'Played')}>Played</div>
                                <div className="col-1 text-center" onClick = {() => this.Sort(0,'Won')}>Won</div>
                                <div className="col-1 text-center" onClick = {() => this.Sort(0,'Lost')}>Lost</div>
                                <div className="col-1 text-center" onClick = {() => this.Sort(0,'Drawn')}>Drawn</div>
                                <div className="col-2 text-center" onClick = {() => this.Sort(0,'GoalsFor')}>Goals For</div>
                                <div className="col-2 text-center" onClick = {() => this.Sort(0,'Goalagainst')}>Goal Against</div>
                                <div className="col-1 text-center" onClick = {() => this.Sort(0,'Points')}>Points</div>
                            </div>
                            {arr.map((val) =>
                            (
                                <div className="row bg-light text-dark border">
                                    <div className="col-3 text-center">{val.Team}</div>
                                    <div className="col-1 text-center">{val.Played}</div>
                                    <div className="col-1 text-center">{val.Won}</div>
                                    <div className="col-1 text-center">{val.Lost}</div>
                                    <div className="col-1 text-center">{val.Drawn}</div>
                                    <div className="col-2 text-center">{val.GoalsFor}</div>
                                    <div className="col-2 text-center">{val.Goalagainst}</div>
                                    <div className="col-1 text-center">{val.Points}</div>

                                </div>
                            ))}
                        </div>
                    }
                </div>
            </React.Fragment>
        );
    }
    ShowAllMatches = () => {
        let arr = this.state.Result_arr;
        return (
            <React.Fragment>
                <div className="container">
                    {
                        arr.length === 0 ? <h6 className="display-6 text-center">There Are No Matches</h6>
                            :
                            <div className="mt-2">
                                <h6 className="display-6 text-center">Result Of The Matches So Far</h6>
                                <div className="row mt-2 bg-dark text-light fw-bold">
                                    <div className="col-3 text-center">Team1</div>
                                    <div className="col-3 text-center">Team2</div>
                                    <div className="col-2 text-center">Score</div>
                                    <div className="col-4 text-center">Result</div>
                                </div>
                                {arr.map((val) =>
                                (
                                    <div className="row bg-light text-dark border">
                                        <div className="col-3 text-center">{val.Team1}</div>
                                        <div className="col-3 text-center">{val.Team2}</div>
                                        <div className="col-2 text-center">{val.goal_a}-{val.goal_b}</div>
                                        <div className="col-4 text-center">
                                            {
                                                val.goal_a > val.goal_b ? <h6 className="h6">{val.Team1} Won</h6>
                                                    : val.goal_a === val.goal_b ? <h6 className="h6">Match Draw</h6>
                                                        : <h6 className="h6">{val.Team2} Won</h6>
                                            }
                                        </div>
                                    </div>
                                ))}
                            </div>

                    }
                </div>
            </React.Fragment>
        );
    }
    MatchOver = () => {
        let set = { ...this.state };
        let iter = { Team1: set.Team_1, Team2: set.Team_2, goal_a: set.Goal_a, goal_b: set.Goal_b,};
        set.Result_arr.push(iter);
        let obj1 = set.PointsTable_arr.find(v => v.Team === set.Team_1);
        let obj2 = set.PointsTable_arr.find(v => v.Team === set.Team_2);
        obj1.Played++;
        obj1.Won += set.Goal_a>set.Goal_b?1:0;
        obj1.Lost += set.Goal_a<set.Goal_b?1:0;
        obj1.Drawn += set.Goal_a === set.Goal_b ?1:0;
        obj1.GoalsFor += set.Goal_a;
        obj1.Goalagainst += set.Goal_b;
        obj1.Points += set.Goal_a>set.Goal_b?2:set.Goal_a === set.Goal_b?1:0;
        obj2.Played++;
        obj2.Won += set.Goal_a<set.Goal_b?1:0;
        obj2.Lost += set.Goal_a>set.Goal_b?1:0;
        obj2.Drawn += set.Goal_b === set.Goal_a ?1:0;
        obj2.GoalsFor += set.Goal_b;
        obj2.Goalagainst += set.Goal_a;
        obj2.Points += set.Goal_b>set.Goal_a?2:set.Goal_a === set.Goal_b?1:0;
        set.Goal_a = 0;
        set.Goal_b = 0;
        set.Team_1 = ""
        set.Team_2 = ""
        set.Main_Index = 0;
        set.Match_counter = set.Match_counter + 1;
        set.Index = 4;
        this.setState(set);
    }
    ScoreInc = (v) => {
        let set = { ...this.state };
        v === 'A' ? set.Goal_a++ : set.Goal_b++;
        this.setState(set);
    }
    MatchStart = () => {
        let x = this.state.Team_1;
        let y = this.state.Team_2;
        let z = this.state.Goal_a;
        let a = this.state.Goal_b;
        return (
            <React.Fragment>
                <div className="mt-2">
                    <h6 className="display-6 text-center fw-bold">Welcome to An Exciting Match</h6>
                    <div className="row">
                        <div className="col-4 text-center">
                            <h6 className="display-6 fw-bold">{x}</h6>
                            <button className="btn btn-warning mt-1 fw-bold" onClick={() => this.ScoreInc('A')}>Goal Scored</button>
                        </div>
                        <div className="col-4 text-center">
                            <h6 className="display-6 text-dark fw-bold">{z} - {a}</h6>
                        </div>
                        <div className="col-4 text-center">
                            <h6 className="display-6 fw-bold">{y}</h6>
                            <button className="btn btn-warning mt-1 fw-bold" onClick={() => this.ScoreInc('B')}>Goal Scored</button>
                        </div>
                    </div>
                    <button className="btn btn-warning d-block mx-auto fw-bold" onClick={() => this.MatchOver()}>Match Over</button>
                </div>
            </React.Fragment>
        );
    }
    Start = () => {
        let x = this.state.Team_1;
        let y = this.state.Team_2;
        if (x === "") {
            alert('Please Select Team 1');
        }
        else if (x === y) {
            alert('Please Select The Different Team')
        }
        else if (y === "") {
            alert('Please Select Team 2');
        }
        else {
            let set = { ...this.state };
            set.Main_Index = 1;
            this.setState(set);
        }
    }
    Event = (v) => {
        let set = { ...this.state };
        set.Team_1 = v;
        this.setState(set);
    }
    Event1 = (v) => {
        let set = { ...this.state };
        set.Team_2 = v;
        this.setState(set);
    }
    ShowNewMatches = () => {
        let arr1 = ['France', 'England', 'Brazil', 'Germany', 'Argentina'];
        let x = this.state.Team_1;
        let y = this.state.Team_2;
        return (
            <React.Fragment>
                <div className="mt-3">
                    {x === "" ? <h6 className="display-6 text-center text-dark fw-bold">Choose Team 1</h6> : <h6 className="display-6 text-center text-dark fw-bold">Team 1:{x}</h6>}
                    <div className="row p-3">
                        {arr1.map((val) => (<div className="col-2 mx-auto"><button className="btn btn-warning fw-bold" onClick={() => this.Event(val)}>{val}</button></div>))}
                    </div>
                    {y === "" ? <h6 className="display-6 text-center text-dark mt-3 fw-bold">Choose Team 2</h6> : <h6 className="display-6 text-center text-dark mt-3 fw-bold">Team 2:{y}</h6>}
                    <div className="row p-3">
                        {arr1.map((val) => (<div className="col-2 mx-auto"><button className="btn btn-warning fw-bold" onClick={() => this.Event1(val)}>{val}</button></div>))}
                    </div>
                    <button className="btn btn-dark text-light d-block mx-auto p-2 mt-3 fw-bold" onClick={() => this.Start()}>Start Match</button>
                </div>
            </React.Fragment>
        )
    }
    Navbar = () => {
        let Match_counter = this.state.Match_counter;
        return (
            <React.Fragment>
                <Navbar Match_counter={Match_counter} />
            </React.Fragment>
        );
    }
    MainFun = (val) => {
        let set = { ...this.state };
        val === 'All Matches' ? set.Index = 1 : val === 'Points Table' ? set.Index = 2 : set.Index = 3;
        this.setState(set);
    }
    Show = () => {
        let Index = this.state.Index;
        let Main_Index = this.state.Main_Index;
        let arr = ['All Matches', 'Points Table', 'New Matches'];
        return (
            <React.Fragment>
                <div className="container p-3">
                    {this.Navbar()}
                    {Main_Index === 1 ? this.MatchStart() :
                        <div className="container m-3">
                            {arr.map((val) =>
                            (
                                <button className="btn btn-primary mx-3" onClick={() => this.MainFun(val)}>{val}</button>
                            ))}
                            {
                                Index === 1 ? this.ShowAllMatches()
                                    : Index === 2 ? this.ShowPointsTable()
                                        : Index === 3 ? this.ShowNewMatches() : ""
                            }

                        </div>
                    }
                </div>
            </React.Fragment>
        );
    }
    render() {
        return this.Show();
    }
}
export default Main;